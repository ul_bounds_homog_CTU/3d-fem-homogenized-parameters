function  homog_sparse(Ntri, example)

close all
E = eye(3);
toler = 1e-9;
max_steps = 12000;
L1 = 2*pi; L2 = 2*pi; L3 = 2*pi; % edges length
N = ones(1,3)*Ntri % voxels
N1 = N(1); N2 = N(2); N3 = N(3); % numbers of small intervals

h1 = L1/N1; h2 = L2/N2; h3 = L3/N3; 
Nnod = (N1+1)*(N2+1)*(N3+1);
Nvox = N1*N2*N3;  % number of voxels
Nele = 6*Nvox;  % number of elements
% reference voxel:
tetra = [0,0,0; 1,0,0; 0,1,0; 0,1,1;
    1,0,0; 0,1,1; 1,0,1; 0,0,1;
    0,0,0; 1,0,0; 0,1,1; 0,0,1;
    1,1,0; 1,0,0; 0,1,0; 0,1,1;
    1,0,0; 0,1,1; 1,0,1; 1,1,1;
    1,1,0; 1,0,0; 0,1,1; 1,1,1];
tetrah = tetra*diag([h1,h2,h3]);
% Drawing (tetrah);

[nodes,peri] = Nodes(N1,N2,N3,L1,L2,L3); % all nodes including boundary

elem = Elem(N1,N2,N3,nodes);

% Drawing(aux);
% CA = zeros(3*Nele); % koefficient matrix
CA_i = [];
CA_j = [];
CA_v = [];
%CAq = zeros(3*Nele); % dual koefficient matrix
CAq_v = [];
%A = zeros(Nnod); % global stiffness matrix
A_i = [];
A_j = [];
A_v = [];
%Aqq = zeros(2*Nnod); % global dual stiffness matrix
Aqq_i = [];
Aqq_j = [];
Aqq_v = [];
B = zeros(Nnod,6); % left side
Bqq = zeros(2*Nnod,3); % right side for the dual problem
%D1 = zeros(Nele,Nnod); % 3 matrices of derivations in the quadrature points
D1_i = [];
D1_j = [];
D1_v = [];
%D2 = zeros(Nele,Nnod);
D2_v = [];
%D3 = zeros(Nele,Nnod);
D3_v = [];
for kk = 1:Nele
    kde = elem(kk,:);
    nod = nodes(kde,:);
    dd = abs(det([nod(1,:)-nod(4,:);nod(2,:)-nod(4,:);nod(3,:)-nod(4,:)]))/6;
    nod1 = [nod,ones(4,1)];
    der = inv(nod1);
    der = der(1:3,:); % d/dx, d/dy, d/dz for 4 basis functions
    g = sum(nod)/4; % centroid
    a = KA(g(1),g(2),g(3), example); 
    aq = inv(a);
    
    pom8 = [kk,kk+Nele,kk+2*Nele];
    %CA(pom8,pom8) = a;
    %CAq(pom8,pom8) = aq;
    ij = repmat(pom8, 3, 1);
    CA_i(end+1:end+3*3) = ij(:);
    ij = ij';
    CA_j(end+1:end+3*3) = ij(:);
    CA_v(end+1:end+3*3) = a(:);
    CAq_v(end+1:end+3*3) = aq(:);
    
    pom = der'*a*der; 

    D1_i(end+1:end+4) = kk;
    D1_j(end+1:end+4) = kde;
    D1_v(end+1:end+4) = der(1,:);
    D2_v(end+1:end+4) = der(2,:);
    D3_v(end+1:end+4) = der(3,:);
    
    %A(kde,kde) = A(kde,kde) + pom*dd;   
    ij = repmat(kde, 4, 1);
    A_i(end+1:end+4*4) = ij(:);
    ij = ij';
    A_j(end+1:end+4*4) = ij(:);
    pom_dd = pom*dd;
    A_v(end+1:end+4*4) = pom_dd(:);
    
    B(kde,1) = B(kde,1)+der'*a*E(:,1)*dd;
    B(kde,2) = B(kde,2)+der'*a*E(:,2)*dd;
    B(kde,3) = B(kde,3)+der'*a*E(:,3)*dd;
    
end;
A = sparse(A_i, A_j, A_v, Nnod, Nnod);
Aqq = sparse(Aqq_i, Aqq_j, Aqq_v, 2*Nnod, 2*Nnod);
CA = sparse(CA_i, CA_j, CA_v, 3*Nele, 3*Nele);
CAq = sparse(CA_i, CA_j, CAq_v, 3*Nele, 3*Nele);
D1 = sparse(D1_i, D1_j, D1_v, Nele, Nnod);
D2 = sparse(D1_i, D1_j, D2_v, Nele, Nnod);
D3 = sparse(D1_i, D1_j, D3_v, Nele, Nnod);

% DD = sparse([D2,D3;-D1,D1*0;D1*0,-D1]);
DQ = sparse([D1*0,-D3,D2;D3,D1*0,-D1;-D2,D1,D1*0]);
D = sparse([D1;D2;D3]);

R = sparse(Nnod,Nvox); % R matrix for transitioning to the periodic domain
per1 = zeros(Nnod,1);
j = 1;
for k1 = 1:N1+1
    for k2 = 1:N2+1
        for k3 = 1:N3+1
R(j,j) = 1;
if (k1==1) R(j+(N2+1)*(N3+1)*N1,j) = 1; end
if (k2==1) R(j+N2*(N3+1),j) = 1; end;
if (k3==1) R(j+N3,j) = 1; end;
if (k1==1 && k2==1) R(j+N1*(N2+1)*(N3+1)+(N3+1)*N2,j) = 1; end;
if (k2==1 && k3==1) R(j+N2*(N3+1)+N3,j) = 1; end;
if (k1==1 && k3==1) R(j+N1*(N2+1)*(N3+1)+N3,j) = 1; end;
if (k1==1 && k2==1 && k3==1) R(j+(N1+1)*(N2+1)*(N3+1)-1,j) = 1; end;
if (k1==N1+1 || k2==N2+1 || k3==N3+1) per1(j) = 1; end;
j = j+1;
        end;
    end
end;
for j = Nnod:-1:1
    if (per1(j)>0) R(:,j) = []; end;
end;
AR = R'*A*R;
BR = -R'*B;

R2 = [R,0*R;0*R,R];
D = D*R;
DQ = DQ*kron(speye(3),R);

% Control:
Aqqx = DQ'*CAq*DQ*dd;
CE = kron(speye(3),ones(Nele,1));
BRx = -D'*CA*CE*dd;
BRqqx = -DQ'*CAq*CE*dd;

% Solution
[U1, ~, ~, st1] = pcg(AR, BR(:,1), toler, max_steps);
Ur1 = reshape(U1,N1,N2,N3);
[U2, ~, ~, st2] = pcg(AR, BR(:,2), toler, max_steps);
Ur2 = reshape(U2,N1,N2,N3);
[U3, ~, ~, st3] = pcg(AR, BR(:,3), toler, max_steps);
Ur3 = reshape(U3,N1,N2,N3);

[Uqq1, ~, ~, stqq1] = pcg(Aqqx, BRqqx(:,1), toler, max_steps);
[Uqq2, ~, ~, stqq2] = pcg(Aqqx, BRqqx(:,2), toler, max_steps);
[Uqq3, ~, ~, stqq3] = pcg(Aqqx, BRqqx(:,3), toler, max_steps);

CGsteps = [st1,st2,st3,stqq1,stqq2,stqq3]

% calculate Astar :
Uall = R*[U1,U2,U3];
Uallqqq = kron(eye(3),R)*[Uqq1,Uqq2,Uqq3];

EAstarE = zeros(3);
EAqqstarE = zeros(3);

% calculate Ah:
U = [U1,U2,U3];

% Uq = [Uq1,Uq2,Uq3];
Uqq = [Uqq1,Uqq2,Uqq3];
Ahx = (D*U+CE)'*CA*(D*U+CE)*dd/pi^3/8;
Bhx2 = (DQ*Uqq+CE)'*CAq*(DQ*Uqq+CE)*dd/pi^3/8;
Bhx2i = inv(Bhx2);
Upper = Ahx
save(sprintf('upper_%d.mat', Ntri), 'Upper');
Lower = Bhx2i
save(sprintf('lower_%d.mat', Ntri), 'Lower');

alfa = Ahx*E;
alfa_size = size(alfa);
inv_alfa = alfa\eye(alfa_size);

W1 = -kron(alfa,ones(Nele,1))+CA*(kron(E,ones(Nele,1))+D*U);

W33 = W1*0;
for k = 1:3
    pom = DQ'*W1(:,k);
    [pom1, ~, ~, st] = pcg(DQ'*DQ, pom, toler, max_steps);
    W33(:,k) = DQ*pom1;
end;
W3 = W33;
pom = W3+kron(alfa,ones(Nele,1));
Cstar2 = pom'*CAq*pom*dd/pi^3/8; 

Astar_lower2_onlyprojection = (inv_alfa*Cstar2*inv_alfa)\eye(alfa_size);

Lower_proj = [Astar_lower2_onlyprojection]

save(sprintf('proj_%d', Ntri), 'Lower_proj');

diff_Upper_Lower = sort(eig(Upper-Lower))

diff_Lower_Lower_proj = sort(eig(Lower-Lower_proj))

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function y = KA(x1,x2,x3, example)

s1 = sin(3/2*x1);
s2 = sin(3/2*x2);
s3 = sin(3/2*x3);

% EXAMPLES:
switch example
    case 1 
        y = [7+sign(s1*s2), -2-sign(s2*s3), sign(s1*s2*s3);
            -2-sign(s2*s3), 4.01+sign(s1*s2), 0;
            sign(s1*s2*s3), 0 , 3+sign(s2*s3)];
    case 2
        y = (2+sign(s1*s2*s3))*diag([1,1,1]);
    
    case 3
        y = diag([1,1,1]);
        if (x1<=pi && x2<=pi || x1>=pi && x2>=pi)
            y = 3*diag([1,1,1]); 
        end
end




% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 

function KresleniNodes(n1,n2,m1,m2,k1,k2,nodes)
figure();
cla; hold on;
n = [n1:n2];
m = [m1:m2];
k = [k1:k2];
plot3(nodes(n,1),nodes(n,2),nodes(n,3),'b.');
plot3(nodes(m,1),nodes(m,2),nodes(m,3),'ro');
plot3(nodes(k,1),nodes(k,2),nodes(k,3),'go');


function elem = Elem(N1,N2,N3,nodes)
NN = N1*N2*N3;
elem = zeros(6*NN,4);
j = 1;
for k1 = 1:N1
    for k2 = 1:N2
        for k3 = 1:N3
        p = (k1-1)*(N2+1)*(N3+1)+(k2-1)*(N3+1)+k3; % left lower corner
        p8 = [p,p+(N2+1)*(N3+1),p+N3+1,p+(N2+2)*(N3+1),...
            p+1,p+(N2+1)*(N3+1)+1,p+N3+2,p+(N2+2)*(N3+1)+1];
            
            elem(j,:) = [p8(1),p8(3),p8(4),p8(8)];
            elem(j+1,:) = [p8(1),p8(5),p8(7),p8(8)];
            elem(j+2,:) = [p8(1),p8(3),p8(7),p8(8)]; 
            
            elem(j+3,:) = [p8(1),p8(2),p8(4),p8(8)];
            elem(j+4,:) = [p8(1),p8(5),p8(6),p8(8)];
            elem(j+5,:) = [p8(1),p8(2),p8(6),p8(8)];
            
            j = j+6;          
        end
    end
end


function [nodes,peri] = Nodes(N1,N2,N3,L1,L2,L3)
NN = (N1+1)*(N2+1)*(N3+1);
h1 = L1/N1; h2 = L2/N2; h3 = L3/N3; 
nodes = zeros(NN,3);
peri = zeros(NN,1);
j = 1;
for k1 = 1:N1+1
    for k2 = 1:N2+1
        for k3 = 1:N3+1
            nodes(j,:) = [(k1-1)*h1,(k2-1)*h2,(k3-1)*h3];
            if (k1>N1+0.5 || k2>N2+0.5 || k3>N3+0.5) peri(j) = 1; end
            j = j+1;            
        end
    end
end


function y = Kresleni(tetrah)
cla; hold on;
P = tetrah(1:4,:); k = boundary(P);
trisurf(k,P(:,1),P(:,2),P(:,3),'Facecolor','red','FaceAlpha',0.1);
P = tetrah(5:8,:); k = boundary(P);
trisurf(k,P(:,1),P(:,2),P(:,3),'Facecolor','blue','FaceAlpha',0.1);
P = tetrah(9:12,:); k = boundary(P);
trisurf(k,P(:,1),P(:,2),P(:,3),'Facecolor','green','FaceAlpha',0.1);
P = tetrah(13:16,:); k = boundary(P);
trisurf(k,P(:,1),P(:,2),P(:,3),'Facecolor','m','FaceAlpha',0.1);
P = tetrah(17:20,:); k = boundary(P);
trisurf(k,P(:,1),P(:,2),P(:,3),'Facecolor','c','FaceAlpha',0.1);
P = tetrah(21:24,:); k = boundary(P);
trisurf(k,P(:,1),P(:,2),P(:,3),'Facecolor','g','FaceAlpha',0.1);

axis([0,3,0,3,0,3]);











