clear all;

% Calculate the bounds for increasing DOFs
N = [6, 12, 24];
max_i = length(N);

for i = 1:max_i
    tic;
    homog_sparse(N(i), 2)
    time_of_1_step = toc
end

%Load the bounds
Upper = zeros(3,3,max_i);
Lower = zeros(3,3,max_i);
Lower_proj = zeros(3,3,max_i);

for i = 1:max_i
    struct = load(sprintf('upper_%d.mat',N(i)));
    Upper(:,:,i) = struct.Upper;
    struct = load(sprintf('lower_%d.mat',N(i)));
    Lower(:,:,i) = struct.Lower;
    struct = load(sprintf('proj_%d.mat',N(i)));
    Lower_proj(:,:,i) = struct.Lower_proj;
end

% Draw
low_y =  [1.70, 1.70, 1.70];
high_y = [1.95, 1.95, 1.95];

for k=1:3
    subplot(2,3,k);
    cla; hold on;
    plot(N.^3,  reshape(Upper(k,k,:),  1, []), '.-r','LineWidth',1,'MarkerSize',10);
    plot(3*N.^3,reshape(Lower(k,k,:),  1, []), '.-b','LineWidth',1,'MarkerSize',10);
    plot(3*N.^3,reshape(Lower_proj(k,k,:), 1, []), '.-g','LineWidth',1,'MarkerSize',10);
    xlabel('DOFs')
    ylabel(sprintf('bounds on A^*_{%d,%d}',k,k))
    set(gca,'xscale','log')
    axis([N(1)^3, 3*N(end)^3, low_y(k), high_y(k)])
end

mat_idx = [1, 2; 1, 3; 2, 3];
low_y =  [-5, -5, -5] * 1e-3;
high_y = [ 0,  0,  0];
for k=1:3
    subplot(2,3,3+k);
    cla; hold on;
    plot(N.^3,  reshape(Upper(mat_idx(k,1),mat_idx(k,2),:),  1, []), '.-r','LineWidth',1,'MarkerSize',10);
    plot(3*N.^3,reshape(Lower(mat_idx(k,1),mat_idx(k,2),:),  1, []), '.-b','LineWidth',1,'MarkerSize',10);
    plot(3*N.^3,reshape(Lower_proj(mat_idx(k,1),mat_idx(k,2),:), 1, []), '.-g','LineWidth',1,'MarkerSize',10);
    xlabel('DOFs')
    ylabel(sprintf('estimates of A^*_{%d,%d}',mat_idx(k,1),mat_idx(k,2)))
    set(gca,'xscale','log')
    axis([N(1)^3, 3*N(end)^3, low_y(k), high_y(k)])
end