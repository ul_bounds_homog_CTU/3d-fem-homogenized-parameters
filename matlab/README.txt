This program is made to efficiently compute lower and upper bounds to the conductivity matrix in a 3D steady-state heat transfer, according to [1].
User can edit the size of the cuboid domain, as well as the function defining local conductivity. The upper and lower bounds are then saved in a .mat file. 
To calculate the Example 1. from [1] run call_and_draw_example_1.m
For Example 2. run call_and_draw_example_2.m
The default maximum number of nodes in one direction is 24. Run larger problems at your own risk, as the code wasn't tested for number of nodes larger than 102.
[1] L. Gaynutdinova, M. Ladecký, A. Nekvinda, I. Pultarová, J. Zeman, Efficient numerical method for reliable upper and lower bounds to homogenized parameters. Submitted. Online: arXiv:2208.09940.